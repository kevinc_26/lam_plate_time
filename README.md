# README #

LAM Plate Time will show plate time efficiency, and debubble count for several LAM prints using the <code>dataframe_log.log</code> file

### How do I get set up and use the script? ###

* Navigate to a home directory and run </br><code>git clone https://kevinc_26@bitbucket.org/kevinc_26/lam_plate_time.git</code>
* Type in the LAM IPs and corresponding ATNs for prints you want to look at
* Run the script

### Who do I talk to? ###

* Kevin