import re
import numpy as np
import matplotlib.pyplot as plt
import os, glob

def float_time(formatted_time):
    """
    Convert formatted time to a float for comparison
    """
    return 60 * 60 * float(formatted_time[0:2]) + 60 * float(formatted_time[3:5]) + float(formatted_time[6:])


if __name__ == '__main__':
    lamIPs = ['192.168.0.115', '192.168.0.115', '192.168.0.115']  # use up to 3
    ATNs = ['2038', '2067', '2062']  # ordered with respect to lamIPs

    fig, ax = plt.subplots()
    fig.subplots_adjust(right=0.75)

    twin1 = ax.twinx()
    twin2 = ax.twinx()

    twin2.spines.right.set_position(("axes", 1.2))

    linestyles = ['-', '.', 'x']
    linestyle_count = 0

    for lamIP, ATNumber in zip(lamIPs, ATNs):
        log_fh = glob.glob('//{}/xilinx/mz_hw/logs/*{}/dataframe_log.log'.format(lamIP, ATNumber))[0]
        with open(log_fh.replace(os.sep, '/')) as f:
            f = f.readlines()

        start_time = float_time(f[0][11:23])
        end_time = float_time(f[-1][11:23])

        layerFLAG = False  # True if starting a new layer and looking for new plate position
        positionFLAG = False  # True if looking for plate position
        plateFLAG = False  # True if looking for next line to get plate time difference

        layer_start_times = []  # list of float start times
        layer_plate_times = []  # list of float plate time per layer
        layer_debubbles = []
        layer_debubble_count = 0
        layer_plate_time = 0

        for line in f:
            # print(line)
            line_time = float_time(line[11:23])
            # print(line_time)

            if 'print.layer_info.number' in line:  # always searching for next layer
                layer_match = re.search("'print.layer_info.number': (\d+)", line)
                print('new layer: {}'.format(int(layer_match.group(1))))
                layer_start_times.append(line_time)
                layerFLAG = True  # look for next stepper.position to get plate position
                plateFLAG = False
                positionFLAG = False
                layer_plate_times.append(layer_plate_time)
                layer_debubbles.append(layer_debubble_count)
                print('layer plate time: {}'.format(layer_plate_time))
                layer_plate_time = 0  # initialize layer plating time
                layer_debubble_count = 0

            elif layerFLAG: # look for plate position for layer
                if 'stepper.position' in line:
                    match = re.search("'stepper.position': (\d+)", line)
                    print('new plate position: {}'.format(int(match.group(1))))
                    plate_position = int(match.group(1))
                    layerFLAG = False
                    plateFLAG = True
                    plate_start = line_time
            elif plateFLAG:
                layer_plate_time += (line_time - plate_start)
                layer_debubble_count += 1
                # print(layer_plate_time, line_time-plate_start, line_time, plate_start)
                plateFLAG = False
                positionFLAG = True
            elif positionFLAG: # look for stepper.position to reach plate position
                if 'stepper.position' in line:
                    match = re.search("'stepper.position': (\d+)", line)
                    if int(match.group(1)) == plate_position:
                        positionFLAG = False
                        plateFLAG = True
                        plate_start = line_time


        layer_debubbles.append(layer_debubble_count)
        layer_debubbles = np.array(layer_debubbles[1:])
        layer_plate_times.append(layer_plate_time)
        layer_plate_times = np.array(layer_plate_times[1:])
        layer_start_times.append(end_time)
        layer_times = np.array(layer_start_times[1:]) - np.array(layer_start_times[0:-1])

        layers = np.arange(1, len(layer_start_times))


        p1, = ax.plot(layers, layer_plate_times, 'b' + linestyles[linestyle_count], label='Layer Plate Times')
        p2, = twin1.plot(layers, layer_debubbles, 'r' + linestyles[linestyle_count], label='Layer Debubble Count')
        p3, = twin2.plot(layers, layer_plate_times/layer_times, 'g' + linestyles[linestyle_count], label='Plating efficiency')

        linestyle_count+=1

        twin2.set_ylim(0,1)

        ax.set_xlabel('Layers')
        ax.set_ylabel('Layer plate times')
        twin1.set_ylabel('Layer Debubbles')
        twin2.set_ylabel('Plating efficiency')

        ax.yaxis.label.set_color(p1.get_color())
        twin1.yaxis.label.set_color(p2.get_color())
        twin2.yaxis.label.set_color(p3.get_color())

        # ax.legend(handles=[p1, p2, p3])

    plt.show()


